/*
 * osc.h
 *
 * Created: 3/5/2019 2:07:26 PM
 *  Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */ 


#ifndef OSC_H_
#define OSC_H_

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdbool.h>

#define SAMPLE_SIZE 250

typedef enum { G1=1, G10, G200 }GainOsc;
typedef enum { MS100=1, MS50, MS20, MS10, MS5, MS2, MS1, MU500, MU250, MU125 }TimeOsc;
	

void oscInit(void);
void oscStart(void);
void oscStop(void);
bool oscIsReady(void);
void oscSetGain(GainOsc);
GainOsc oscGetGain(void);
void oscSetTime(TimeOsc);
TimeOsc oscGetTime(void);
int8_t* oscGetData(uint16_t);
void oscSetData(uint16_t, int8_t);
void oscCopyData(void);

#endif /* OSC_H_ */