/*
 * atbOscilloscope.c
 *
 * Created: 3/4/2019 12:16:45 PM
 * Author : BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <stdbool.h>
#include <string.h>
#include "sh1106/sh1106.h"
#include "osc.h"

#define KEY_PORT	PORTD
#define KEY_PIN		PIND
#define KEY_MASK	0b11110000

typedef enum { UP, DOWN, MODE_UP, MODE_DOWN	, NONE }Key;
typedef enum { VOLTMETER, TIME, GAIN, TRIGGER }State;
	
void keysInit(void);
void voltmeter(Key);
TimeOsc timeFun(Key);
GainOsc gainFun(Key);
int8_t triggerFun(Key,int8_t);
void drawOsc(int8_t *buff, int8_t trig);
void findTrigger(int8_t *, int8_t);

const char timStr[10][6] =
{
	"100ms",
	"50ms",
	"20ms",
	"10ms",
	"5ms",
	"2ms",
	"1ms",
	"500us",
	"250us",
	"125us",
};

const char gainStr[3][6] =
{
	"1.28V",
	"128mV",
	"6.4mV",
};

volatile Key currentKey = NONE;
State currentState = TRIGGER;


int main(void)
{
	KEY_PORT = KEY_MASK;
	sh1106_init(REFRESH_MAX,0);
	oscInit();
	keysInit();
	sei();
		
	bufor_clear();
	draw_bufor_string( (128-6*12)/2, 5, "oscilloscope", 1, 0, 1 );
	draw_bufor_string( (128-6*16)/2, 15, "sampling 256kSps", 1, 0, 1 );
	draw_bufor_string( (128-6*19)/2, 25, "author: Piotr Zmuda", 1, 0, 1 );
	draw_bufor_string( (128-6*15)/2, 63-7, "<press any key>", 1, 0, 1 );
	sh1106_display();
	while( (KEY_PIN & KEY_MASK) == KEY_MASK ) {}
	
	TimeOsc time = MS10;
	GainOsc gain = G10;
	int8_t trig = 0;
	int8_t trigLast = 0;
	
	for(;;)
	{	
		Key tmpKey = currentKey;
		
		if( tmpKey != NONE )
			currentKey = NONE;
		
		if( VOLTMETER == currentState )
			voltmeter(tmpKey);
		else if( TIME == currentState )
			time = timeFun(tmpKey);
		else if( GAIN == currentState )
			gain = gainFun(tmpKey);
		else if( TRIGGER == currentState )
			trig = triggerFun(tmpKey,trigLast);
		
		if( VOLTMETER != currentState )
		{
			static int8_t buf[128];
			
			if( oscGetGain() != gain || oscGetTime() != time )
			{
				oscStop();
				oscSetGain(gain);
				oscSetTime(time);
				oscStart();
				
				memset(buf, 0, 128);
				drawOsc(buf,trig);
			}
			
			if( oscIsReady() == true )
			{
				oscCopyData();
				findTrigger( buf, trig );
				oscStart();
				drawOsc( buf, trig );
			}
			else if( trigLast != trig )
			{
				findTrigger( buf, trig );
				drawOsc( buf, trig );
			}
		}
		
		trigLast = trig;
	}
}

ISR(TIMER0_COMPA_vect)
{
	const uint8_t CNT_PRESS = 3;
	static uint8_t cnt = 0;
	static Key last = NONE;
	Key current;
	
	if( !(KEY_PIN & 0b10000000) )
		current = MODE_DOWN;
	else if( !(KEY_PIN & 0b01000000) )
		current = MODE_UP;
	else if( !(KEY_PIN & 0b00100000) )
		current = DOWN;
	else if( !(KEY_PIN & 0b00010000) )
		current = UP;
	else
		current = NONE;
	
	if( last == current && current != NONE )
	{
		if( cnt >= CNT_PRESS )
		{
			currentKey = current;
			cnt = 0;
		}
		else
			cnt++;
	}
	else
		cnt = 0;
	
	last = current;
}

void keysInit(void)
{
	TCCR0A |= (1<<WGM01);					// ctc mode
	TCCR0B |= (1<<CS02)|(1<<CS00);			// set the prescaler for timer0 to 1024
	OCR0A = F_CPU/1024/50-1;				// xtal/prescaler/50Hz
	TIMSK0 |= (1<<OCIE0A);					// enable compare A intterupt for timer0
}

void voltmeter(Key k)
{
	if( k == MODE_UP )
		currentState = TIME;
	else if( k == MODE_DOWN )
		currentState = TRIGGER;
	else
	{
		GainOsc tmp = oscGetGain();
		int32_t val = 0;
		
		if( k == UP )
		{
			tmp++;

			if( tmp >= G200 )
				tmp = G200;
		}
		else if( k == DOWN )
		{
			tmp--;

			if( tmp <= G1 )
				tmp = G1;
		}
		
		oscStop();
		oscSetGain((GainOsc)tmp);
		oscSetTime(MU500);
		oscStart();
		while( oscIsReady() == false ) {};
			oscCopyData();
		
		for( uint16_t i=0; i<SAMPLE_SIZE; i++ )
			val = val + *oscGetData(i);
		
		char strV[3];
		char strVMax[8];
		
		if( oscGetGain() == G200 )
		{
			strcpy(strV, "uV");
			strcpy(strVMax, "12800uV");
			val = 100 * val / SAMPLE_SIZE;
		}
		else
		{
			strcpy(strV, "mV");
			
			if( oscGetGain() == G1 )
			{
				strcpy(strVMax, "2560mV");
				val = 20 * val / SAMPLE_SIZE;
			}
			else if( oscGetGain() == G10 )
			{
				strcpy(strVMax, "256mV");
				val = 2 * val / SAMPLE_SIZE;
			}
		}
		
		bufor_clear();
		draw_bufor_string( 0, 57, "VOLTMETER", 0, 1, 1 );
		draw_bufor_int( 0, 14, val, 1, 0, 3 );
		draw_bufor_string( cursor_x+1, 14, strV, 1, 0, 3 );
		draw_bufor_string( 0, 0, strVMax, 1, 0, 1 );
		sh1106_display();
	}
}

TimeOsc timeFun(Key k)
{
	TimeOsc tmp = oscGetTime();
	
	if( k == MODE_UP )
		currentState = GAIN;
	else if( k == MODE_DOWN )
		currentState = VOLTMETER;
	else
	{
		if( k == UP )
		{
			tmp++;

			if( tmp >= MU125 )
				tmp = MU125;
		}
		else if( k == DOWN )
		{
			tmp--;

			if( tmp <= MS100 )
				tmp = MS100;
		}
	}	
	
	return tmp;
}

GainOsc gainFun(Key k)
{
	GainOsc tmp = oscGetGain();
	
	if( k == MODE_UP )
		currentState = TRIGGER;
	else if( k == MODE_DOWN )
		currentState = TIME;
	else
	{
		if( k == UP )
		{
			tmp++;

			if( tmp >= G200 )
				tmp = G200;
		}
		else if( k == DOWN )
		{
			tmp--;

			if( tmp <= G1 )
				tmp = G1;
		}
	}
	
	return tmp;
}

int8_t triggerFun( Key k, int8_t t)
{
	if( k == MODE_UP )
		currentState = VOLTMETER;
	else if( k == MODE_DOWN )
		currentState = GAIN;
	else
	{
		if( k == UP )
		{
			t += 5;

			if( t >= 120 )
			t = 120;
		}
		else if( k == DOWN )
		{
			t -= 5;

			if( t <= -120 )
				t = -120;
		}
	}
	
	return t;
}

void drawOsc(int8_t *buff, int8_t trig)
{
	bufor_clear();
	
	draw_bufor_vline( 63, 0, 63, 1 );
	draw_bufor_hline( 0, 127, 31, 1 );
	
	draw_bufor_vline( 63-2*(128/5), 27, 34, 1 );
	draw_bufor_vline( 63-1*(128/5), 27, 34, 1 );
	draw_bufor_vline( 63+1*(128/5), 27, 34, 1 );
	draw_bufor_vline( 63+2*(128/5), 27, 34, 1 );
	
	draw_bufor_hline( 59, 67, 15, 1 );
	draw_bufor_hline( 59, 67, 47, 1 );
	
	for( uint8_t i=0; i<127; i++ )
	{
		if( i % 3 == 0 )
			draw_bufor_pixel( i, 31-trig/4, 1 );
			
		draw_bufor_line( i, 31-buff[i]/4, i+1, 31-buff[i+1]/4, 1 );
	}
	
	if( currentState == TIME )
		draw_bufor_string( 0, 57, "TIMER", 0, 1, 1 );
	else if( currentState == GAIN )
		draw_bufor_string( 0, 57, "GAIN", 0, 1, 1 );
	else if( currentState == TRIGGER )
		draw_bufor_string( 0, 57, "TRIGGER", 0, 1, 1 );
	
	draw_bufor_string( 0, 0, timStr[oscGetTime()-1], 0, 1, 1 );
	draw_bufor_string( cursor_x, 0, gainStr[oscGetGain()-1], 0, 1, 1 );
			
	sh1106_display();
}

uint16_t findEdge(int8_t trig)
{
	uint16_t cursor = 64;
	
	for( ; cursor <= SAMPLE_SIZE-64 && *oscGetData(cursor) >= trig; cursor++ ) {}
	for( ; cursor <= SAMPLE_SIZE-64 && *oscGetData(cursor) <= trig; cursor++ ) {}
	
	return cursor;
}

void findTrigger(int8_t *buff, int8_t trig)
{
	uint16_t cursor = findEdge(trig);
	int8_t *tmp;
	
	if( cursor >= SAMPLE_SIZE-64 )	
		tmp = oscGetData(0);
	else
		tmp = oscGetData(cursor-64);
		
	memcpy( buff, tmp, 128 );
}