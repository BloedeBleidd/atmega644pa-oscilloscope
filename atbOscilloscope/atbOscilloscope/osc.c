/*
 * osc.c
 *
 * Created: 3/5/2019 2:07:15 PM
 *  Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */ 

#include <string.h>
#include "osc.h"

static volatile uint16_t currentSample = 0;
volatile bool isReady = true;
volatile int8_t sampleBuffer[SAMPLE_SIZE];
int8_t sampleBuffer2[SAMPLE_SIZE];

static GainOsc currentGain = G1; 
static TimeOsc currentTime = MS50; 

void adcInit(void)
{
	ADMUX = (1<<REFS0) | (1<<REFS1) | (1<<ADLAR);	// internal ref as 2,56V / left adjust /
	ADCSRA = (1<<ADEN) | (1<<ADATE) | (1<<ADIE) | (1<<ADPS0) | (1<<ADPS2); // enable / auto trigger / interrupt enable / prescaler 32x
	ADCSRA |= (1<<ADIF); // clear interrupt flag
	ADCSRB = (1<<ADTS0) | (1<<ADTS2);	// source as TIM1COMPB interrupt
}

void adcGainReset(void)
{
	ADMUX &= ~( (1<<MUX0) | (1<<MUX1) | (1<<MUX2) | (1<<MUX3) | (1<<MUX4) );
}

void adcGain(void)
{
	adcGainReset();
	ADMUX |= (1<<MUX0) | (1<<MUX1);
}

void adcGain1(void)
{
	adcGainReset();
	ADMUX |= (1<<MUX0) | (1<<MUX1) | (1<<MUX3) | (1<<MUX4);
}

void adcGain10(void)
{
	adcGainReset();
	ADMUX |= (1<<MUX0) | (1<<MUX2) | (1<<MUX3);
}

void adcGain200(void)
{
	adcGainReset();
	ADMUX |= (1<<MUX0) | (1<<MUX1) | (1<<MUX2) | (1<<MUX3);
}

void adcStart(void)
{
	ADCSRA |= (1<<ADSC);
}

void timInit(void)
{
	TCCR1B = (1 << WGM12);
}

void timStart(void)
{
	TCNT1 = 0;
	TCCR1B |= (1 << CS10);
}

void timStop(void)
{
	TCCR1B &= ~( (1 << CS10) | (1 << CS11) | (1 << CS12) );
}

void timCntReset(void)
{
	TCNT1 = 0;
}

void timFlagReset(void)
{
	TIFR1 |= (1<<OCF1A) | (1<<OCF1B);
}

void timCompSet( uint16_t comp )
{
	OCR1A = comp;
	OCR1B = comp;
}

void oscInit(void)
{
	adcInit();
	timInit();
}

void oscStart(void)
{
	isReady = false;
	timStart();
}

void oscStop(void)
{
	isReady = true;
	timStop();
	timFlagReset();
	timCntReset();
	currentSample = 0;
}

bool oscIsReady(void)
{
	return isReady;
}

void oscSetGain(GainOsc g)
{
	currentGain = g;
	
	if( g == G1 )
		adcGain1();
	else if( g == G10 )
		adcGain10();
	else
		adcGain200();	
}

GainOsc oscGetGain(void)
{
	return currentGain;
}

void oscSetTime(TimeOsc t)
{
	const uint32_t TMP = F_CPU*5/128;
	uint16_t tmp;
	
	currentTime = t;
	
	if( t == MS100 )
		tmp = 10;
	else if( t == MS50 )
		tmp = 20;
	else if( t == MS20 )
		tmp = 50;
	else if( t == MS10 )
		tmp = 100;
	else if( t == MS5 )
		tmp = 200;
	else if( t == MS2 )
		tmp = 500;
	else if( t == MS1 )
		tmp = 1000;
	else if( t == MU500 )
		tmp = 2000;
	else if( t == MU250 )
		tmp = 4000;
	else if( t == MU125 )
		tmp = 8000;
	else
		tmp = 1;

	timCompSet( TMP/tmp-1 );
}

TimeOsc oscGetTime(void)
{
	return currentTime;
}

int8_t* oscGetData(uint16_t n)
{
	return sampleBuffer2+n;
}

void oscSetData(uint16_t n, int8_t d)
{
	*(sampleBuffer2+n) = d;
}

ISR(ADC_vect)
{
	timFlagReset();
	
	if( currentSample >= SAMPLE_SIZE )
	{
		timStop();
		timFlagReset();
		timCntReset();
		currentSample = 0;
		isReady = true;
	}
	else
	{
		sampleBuffer[currentSample] = ADCH;
		currentSample++;
	}
}

void oscCopyData(void)
{
	memcpy(sampleBuffer2,(const char*)sampleBuffer,SAMPLE_SIZE);
}