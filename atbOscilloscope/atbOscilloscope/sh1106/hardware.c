/*
 * hardware.c
 *
 *  Created on: 2 lip 2015
 *      Author: BloedeBleidd - Piotr Zmuda bloede.bleidd97@gmail.com
 */

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <string.h>
#include <stdlib.h>

#include "sh1106.h"

#define SPIF_FLAG (SPSR & (1<<SPIF))

void SPI_Init (void)
{
	DDRx |= ( MOSI ) | ( SCK ) | ( DC );
	SPSR |= ( 1<<SPI2X );// | ( 1<<SPR1 );
	SPCR |= ( 1<<SPE ) | ( 1<<MSTR ) | ( 1<<CPOL ) | ( 1<<CPHA );
}

void SPI_Send (uint8_t data)
{
	SPDR = data;
	while( !SPIF_FLAG ) {}
}

void sh1106_cmd (uint8_t cmd)
{
	#if USE_CS == 1
	CS_HIGH;
	#endif

	DC_LOW;

	#if USE_CS == 1
	CS_LOW;
	#endif

	SPI_Send (cmd);

	#if USE_CS == 1
	CS_HIGH;
	#endif
}

void sh1106_data (uint8_t data)
{
	#if USE_CS == 1
	CS_HIGH;
	#endif

	DC_HIGH;

	#if USE_CS == 1
	CS_LOW;
	#endif

	SPI_Send (data);

	#if USE_CS == 1
	CS_HIGH;
	#endif
}

void sh1106_init (uint8_t refresh, uint8_t contrast)
{
	SPI_Init();

	#if USE_RST == 1
	RST_DDR |= RST;

	RST_HIGH;
	_delay_us(25);
	RST_LOW;
	_delay_us(25);
	RST_HIGH;
	#endif

	#if USE_CS == 1
	RST_DDR |= RST;
	RST_HIGH;
	CS_DDR |= CS;
	CS_HIGH;
	_delay_ms(50);
	CS_LOW;
	_delay_ms(50);
	CS_HIGH;
	#endif

	CS_LOW;
	DC_LOW;

	SPI_Send (SH1106_DISPLAYOFF);
	SPI_Send (SH1106_SETMULTIPLEX);
	SPI_Send (0x3F);
	SPI_Send (SH1106_SETSTARTLINE);        /*set display start line*/
	SPI_Send (SH1106_PAGEADDR);   		 	/*set page address*/
	SPI_Send (SH1106_SETLOWCOLUMN|0x02);	/*set lower column address*/
	SPI_Send (SH1106_SETHIGHCOLUMN);    	/*set higher column address*/
	SPI_Send (SH1106_SEGREMAP|0x01); 		/*set segment remap*/
	SPI_Send (SH1106_NORMALDISPLAY);    	/*normal / reverse*/
	SPI_Send (SH1106_CHARGEPUMP);			/*set charge pump enable*/
	SPI_Send (SH1106_MEMORYMODE);
	SPI_Send (0x00);
	SPI_Send (SH1106_EXTERNALVCC);			/*external VCC   */
	SPI_Send (0x31);  						/*0X30---0X33  7,4V-8V-8,4V-9V */
	_delay_ms(20);
	SPI_Send (SH1106_COMSCANDEC);    		/*Com scan direction*/
	SPI_Send (SH1106_SETDISPLAYOFFSET);    /*set display offset*/
	SPI_Send (0x00);   						/*   0x20  /   0x00 */
	SPI_Send (SH1106_SETDISPLAYCLOCKDIV);  /*set osc division*/
	SPI_Send (refresh);
	SPI_Send (SH1106_SETPRECHARGE);    	/*set pre-charge period*/
	SPI_Send (0x1f);    					/*0x22  /  0x1f*/
	SPI_Send (SH1106_SETCOMPINS);    		/*set COM pins*/
	SPI_Send (0x12);
	SPI_Send (SH1106_SETVCOMDETECT);    	/*set vcomh*/
	SPI_Send (0x40);
	SPI_Send (SH1106_SETCONTRAST);
	SPI_Send (contrast);

	SPI_Send (SH1106_DISPLAYALLON_RESUME);
	SPI_Send (SH1106_NORMALDISPLAY);
	// wait 100ms
	_delay_ms(50);
	// turn on oled panel
	SPI_Send (SH1106_DISPLAYON);

	CS_HIGH;

	bufor_clear();
	sh1106_display();
}

void sh1106_display (void)
{
	#if USE_CS == 1
		CS_LOW;
	#endif

	DC_LOW;
	SPI_Send (SH1106_SETLOWCOLUMN);
	
	for( uint8_t n=0; n<8; n++ )
	{
		DC_LOW;
		SPI_Send ( 0xB0 | n );
		SPI_Send (SH1106_SETHIGHCOLUMN);
		DC_HIGH;
		for( uint8_t i=0; i<128; i++ ) { SPI_Send(bufor[i+128*n]); }
	}

	#if USE_CS == 1
		CS_HIGH;
	#endif
}